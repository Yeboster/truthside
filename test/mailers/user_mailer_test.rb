require "test_helper"

class UserMailerTest < ActionMailer::TestCase
  def setup
    @user = users(:user)
    @user.activation_token = User.new_token
  end

  test "account_activation" do
    mail = UserMailer.account_activation @user
    assert_equal "Account activation | TruthSide", mail.subject
    assert_equal [@user.email], mail.to
    assert_equal ["noreply@yeboster.club"], mail.from
    assert_match @user.name, mail.body.encoded
    assert_match @user.activation_token, mail.body.encoded
    assert_match CGI.escape(@user.email), mail.body.encoded
  end

  test "password_reset" do
    @user.reset_token = User.new_token
    mail = UserMailer.password_reset(@user)
    assert_equal "Password Reset | TruthSide", mail.subject
    assert_equal [@user.email], mail.to
    assert_equal ["noreply@yeboster.club"], mail.from
    assert_match "Hi", mail.body.encoded
    assert_match @user.reset_token, mail.body.encoded
  end
end
