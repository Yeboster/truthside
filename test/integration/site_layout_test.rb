require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links" do
    get root_path
    assert_template "static_pages/home"
    # the '?' is used to insert the  variable and if necessary it escape special characters from the section
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
  end

  test "title links" do
    base_title = 'Truth Side'
    get root_path
    assert_select "title", text: "#{base_title}"
    get helf_path
    assert_select "title", text: "About | #{base_title}"
    get contact_path
    assert_select "title", text: "Contact | #{base_title}"
    # exercise on the rails tutorial
    assert_select "title", full_title("Contact")
    get about_path
    assert_select "title", text: "About | #{base_title}"
  end
    
end
