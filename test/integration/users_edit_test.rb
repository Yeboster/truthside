require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:obey)
  end

  test 'unsuccessful edit' do
    log_in_as @user
    get edit_user_path @user
    assert_template 'users/edit'
    patch user_path @user, params: {
      user: {
        name: "",
        password: "foo",
        password_confirmation: "bar"
      }
    }
    assert_select 'div#message_explanation'
    assert_template 'users/edit'
  end

  test 'successful edit' do
    log_in_as @user
    get edit_user_path @user
    assert_template 'users/edit'
    name = 'Yo Boy'
    email = 'loveme@yeboster.club'
    patch user_path @user, params: {
      user: {
        name: name,
        email: email,
        password: "",
        password_confirmation: ""
      }
    }
    assert_not flash.empty?
    assert_redirected_to @user
    # updating user changes
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
end
