require 'test_helper'

class PostsInterfaceTest < ActionDispatch::IntegrationTest
 
  def setup
    @user = users(:obey)
    log_in_as @user
    @yebo = users(:yebo)
  end

  test "post interface" do
    log_in_as(@user)
    get root_path
    assert_select 'ul'
    # Invalid submission
    assert_no_difference 'Post.count' do
      post posts_path, params: { post: { content: "", title: "" } }
    end
    # Valid submission
    title = "I love myself"
    content = "Obey to Yeboster"
    assert_difference 'Post.count', 1 do
      post posts_path, params: { post: { title: title, content: content } }
    end
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    # Delete post
    first_post = @user.posts.paginate(page: 1).first
    assert_difference 'Post.count', -1 do
      delete post_path(first_post)
    end
    # Visit different user (no delete links)
    get user_path(users(:yebo))
    assert_select 'a', text: 'delete', count: 0
  end

  test "post sidebar count" do
    get root_path
    assert_match "8 Posts", response.body
    # User with zero microposts
    other_user = users(:yebo)
    log_in_as(other_user)
    get root_path
    assert_match "0 Posts", response.body
    other_user.posts.create!(title: "A title", content: "A little post")
    get root_path
    assert_match "1 Posts", response.body
  end

  test "post followers counter" do
    get root_path
    assert_match @user.followers.count.to_s, response.body
    assert_match @user.following.count.to_s, response.body
  end
end
