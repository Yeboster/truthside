require "test_helper"

class SignupUsersTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "user signup should not be fine" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: {
        user: {
          name:                  "",
          email:                 "invalid@",
          password:              "yo",
          password_confirmation: "boy"
        }
      }
      assert_template "users/new"
      assert_select "div#message_explanation"
      assert_select "div.alert"
    end
  end

  test "user signup successful" do
    get signup_path
    base = ("a".."z").to_a.sample(11).join
    assert_difference "User.count" do
      post users_path, params: {
        user: {
          name:                  base,
          email:                 "#{base}@yeboster.club",
          password:              "yomate,I'mfine",
          password_confirmation: "yomate,I'mfine"
        }
      }
    end
    follow_redirect!
    # assert_template "users/show"
    # # using a test helper to check if automatically a test user was signed in
    # assert is_logged?
  end

  test "valid signup with activation" do
    get signup_path
    assert_difference "User.count", 1 do
      post users_path, params: {
        user: {
          name:                  "Hello",
          email:                 "its@m.e",
          password:              "again,meme",
          password_confirmation: "again,meme"
        }
      }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    # Try to login
    log_in_as user
    assert_not is_logged?
    # invalid activation token
    get edit_account_activation_path "invalid token", email: user.email
    assert_not is_logged?
    # valid token, wrong email
    get edit_account_activation_path user.activation_token, email: "wrong"
    assert_not is_logged?
    # valid activation
    get edit_account_activation_path user.activation_token, email: user.email
    assert user.reload.activated?
    follow_redirect!
    assert_template "users/show"
    assert is_logged?
  end
end
