require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = User.find_by(email: "admin@yeboster.club")
  end

  test "should be a bad authentication" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { email: "", password: "" } } 
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "should be a good authentication" do
    get login_path
    post login_path, params: { 
      session: { 
        email: @user.email,
        password: "aaaaaaaa", 
      } 
    }
    # check if there is an redirect
    assert_redirected_to @user
    # actually going to the redirected page
    follow_redirect!
    assert_template 'users/show'
    # check if the login path disappear
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
  end

  test "login with valid information followed by logout" do
    get login_path

    log_in_as @user

    assert is_logged?
    assert_redirected_to @user
    # same as: assert_redirected_to users_path(@user)
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged?
    assert_redirected_to root_path
    # simulating other browser
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "login with remembering" do
    log_in_as @user
    assert_equal cookies['remember_token'], assigns(:user).remember_token
    assert_not_empty cookies['remember_token']
  end

  test "login without remembering" do
    log_in_as @user
    log_in_as @user, remember_me: '0'
    assert_empty cookies['remember_token']
  end
end
