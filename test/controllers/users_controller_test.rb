require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:obey)
    @other = users(:yebo)
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  test 'should redirect edit when not logged in' do
    get edit_user_path @user
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test 'should redirect update when not logged in' do
    patch user_path @user, params: {
      user: {
        name: @user.name,
        email: @user.email
      }
    }
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test 'should redirect index when not logged in' do
    get users_path
    assert_redirected_to login_path
  end

  test 'should redirect edit when not logged in as WRONG user' do
    get edit_user_path @other
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test 'should redirect update when not logged in as WRONG user' do
    patch user_path @other, params: {
      user: {
        name: @other.name,
        email: @other.email
      }
    }
    assert_not flash.empty?
    assert_redirected_to login_path
  end
  
  test 'should not be allowed to patch the admin attribute' do
    log_in_as @other
    assert_not @other.admin?
    patch user_path(@other), params: {
      user: {
        password: 'aaaaaaaa',
        password_confirmation: 'aaaaaaaa',
        admin: true
      }
    }
    assert_not @other.reload.admin?
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path @user 
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as @other 
    assert_no_difference 'User.count' do
      delete user_path @user 
    end
    assert_redirected_to error_path
  end

  test 'an admin should destroy an user' do
    log_in_as @user 
    # this is strange
    assert_difference 'User.count', -1 do
      delete user_path @other 
    end
  end
  
  test "should redirect following when not logged in" do
    get following_user_path(@user)
    assert_redirected_to login_url
  end

  test "should redirect followers when not logged in" do
    get followers_user_path(@user)
    assert_redirected_to login_url
  end
end
