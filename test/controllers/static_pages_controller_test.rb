require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def default
    @base_title = "Truth Side"
  end

  test "should get root" do
    # You can also get pages with path
    # get '/static_pages/about'
    get root_path
    assert_response :success
    assert_select "title", default
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | "+default
  end
  
  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | "+default
  end
end
