require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.new name: "Yeboster", email: "loveme@yeboster.club", password: "xxxxxxxx", password_confirmation: "xxxxxxxx"
  end

  def update(email)
    @user.update_attributes email: email
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    n = "yebo"
    @user.name = "   "
    # checks if the string is blank and negate the statement
    # assert_not @user.blank?
    assert_not @user.valid?
  end

  test "email should be present" do
    m = "obey"
    @user.email = "   "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    n = "yebo"
    @user.name = (n + n.reverse) * 5
    # checks if the string is blank and negate the statement
    # assert_not @user.blank?
    assert_not @user.valid?
  end

  test "email should not be too long" do
    m = "obey"
    @user.email = (m + m.reverse)* 20 + "@yeboster.club"
    assert_not @user.valid?
  end

  test "email validation for different mail types" do
    emails = %w[user@example.com thegreatest@yeboster.club]
    emails.each do |e|
      update(e)
      assert @user.valid?, "#{emails.inspect} should be valid"
    end
  end

  test "email validation should be reject invalid mail types" do
    emails = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com]
    emails.each do |invalid_mail|
      update(invalid_mail)
      # inspect puts "" on the variables
      # when the result is false the assertion it thrown
      assert_not @user.valid?, "#{invalid_mail.inspect} should be rejected"
    end
  end

  test "email duplication should be rejected" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.downcase
    @user.password = password_confirmation = "xxxxxxxx"
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "LOVeME@yeboster.club"
    @user.email = mixed_case_email
    @user.password = password_confirmation = "xxxxxxxx"
    @user.save
    @user.reload
    assert_equal mixed_case_email.downcase, @user.email
  end

  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "associated posts should be deleted" do
    @user.save
    @user.posts.create! title: "Love", content: "Yeboster"
    assert_difference 'Post.count', -1 do
      @user.destroy
    end
  end

end
