require 'test_helper'

class PostTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:obey)
    @post = @user.posts.build title: "Lov u", content: "Yo boy!!"
  end

  test "should be valid" do
    assert @post.valid?
  end

  test "user id should be present" do
    @post.user_id = nil
    assert_not @post.valid?
  end

  test "content should be present" do
    @post.content = "   "
    assert_not @post.valid?
  end

  test "content should be at minimum 4 characters" do
    @post.content = "a" * 3
    assert_not @post.valid?
  end

  test "content should be at most 255 characters" do
    @post.content = "a" * 256
    assert_not @post.valid?
  end

  test "order should be most recent first" do
    assert_equal posts(:most_recent), Post.first
  end
  
end
