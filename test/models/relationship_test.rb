require "test_helper"

class RelationshipTest < ActiveSupport::TestCase
  def setup
    @relationship = Relationship.new(follower_id: users(:obey).id,
                                     followed_id: users(:yebo).id)
  end

  test "should be valid" do
    assert @relationship.valid?
  end

  test "should require a follower_id" do
    @relationship.follower_id = nil
    assert_not @relationship.valid?
  end

  test "should require a followed_id" do
    @relationship.followed_id = nil
    assert_not @relationship.valid?
  end

  test "should follow and unfollow a user" do
    obey = users(:obey)
    yebo = users(:yebo)
    assert obey.following?(yebo)
    assert yebo.followers.include?(obey)
    obey.unfollow(yebo)
    assert_not obey.following?(yebo)
  end
end
