Rails.application.routes.draw do
  get "password_resets/new"

  get "password_resets/edit"

  # root page
  root "static_pages#home"
  # static pages routes
  get "/help",    to: "static_pages#about", as: "helf"
  get "/about",   to: "static_pages#about"
  get "/contact", to: "static_pages#contact"
  # for new user
  get "/signup", to: "users#new"
  post "/signup", to: "users#create"
  # for sign in
  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  delete "/logout", to: "sessions#destroy"
  # show all user's posts
  get "/user/posts/:id", to: "posts#posts", as: "user_posts"
  # error page
  get "/error", to: "static_pages#error"

  # adding REST architecture
  resources :users do
    # member return id
    # collection returns the obj
    member do
      get :following, :followers
    end
  end

  # Resources for the reset password and activation path
  resources :account_activations, only: :edit
  resources :password_resets,     only: %i[new edit update create]
  resources :posts,               only: %i[create destroy]
  resources :relationships,       only: %i[create destroy]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
