require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TruthSide
  class Application < Rails::Application
    # adding fonts to the pipeline
    # config.assets.paths << Rails.root.join("app", "assets", "fonts")

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    # Include the authenticity token in remote forms
    config.action_view.embed_authenticity_token_in_remote_forms = true

    config.autoload_paths += %W[#{Rails.root}/app/models/postcard #{Rails.root}/config/initializers/version.rb]

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
