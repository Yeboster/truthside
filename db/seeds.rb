def_bio = "I'am proud to be an user of Truth Side, aren't you?"
User.create!(
  name:                  "Yeboster",
  email:                 "admin@yeboster.club",
  password:              "password",
  password_confirmation: "password",
  admin:                 true,
  activated:             true,
  activated_at:          Time.zone.now,
  bio:                   "Obey always to your soul's voice"
)

User.create!(
  name:                  "Wilfred Smith Adarkwaah",
  email:                 "willy@yeboster.club",
  password:              "password",
  password_confirmation: "password",
  admin:                 true,
  activated:             true,
  activated_at:          Time.zone.now,
  bio:                   def_bio
)

unless Rails.env.development?
  # Creating users

  88.times do
    name = Faker::Name.name
    email = ("a".."z").to_a.shuffle.join[0..8] + "@yeboster.club"
    pass = "password"
    User.create!(
      name:                  name,
      email:                 email,
      password:              pass,
      password_confirmation: pass,
      activated:             true,
      activated_at:          Time.zone.now,
      bio:                   def_bio
    )
  end

  # Adding posts to 6 users

  users = User.order(:created_at).take(6)
  88.times do
    t = Faker::Lorem.words.join(" ")
    c = Faker::Lorem.sentence(10)
    users.each { |u| u.posts.create! title: t, content: c }
  end

  # Creting relationships between users

  users = User.all
  user = users.first
  following = users[2..50]
  followers = users[2..70]
  # The user is following
  following.each { |followed| user.follow(followed) }
  # The user is followed
  followers.each { |follower| follower.follow(user) }
end
