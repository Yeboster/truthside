class AddTrackableToUser < ActiveRecord::Migration[5.1]
  def change
    change_table :users do |t|
      t.integer :sign_in_count, default: 0
      t.datetime :current_sign_in_at
      t.string :current_sign_in_ip
    end
  end
end
