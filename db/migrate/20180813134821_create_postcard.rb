class CreatePostcard < ActiveRecord::Migration[5.1]
  def change
    create_table :postcards do |t|
      t.string :type, null: false
      t.json :sti
      t.string :picture
      t.references :user, foreign_key: true
      t.timestamps
    end
    add_index :postcards, %i[user_id created_at]
  end
end
