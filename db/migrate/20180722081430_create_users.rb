class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    # this is a block!
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :avatar
      t.text :bio
      t.timestamps
    end
  end
end
