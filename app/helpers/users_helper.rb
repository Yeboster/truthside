module UsersHelper
  def gravatar_for(user, size: "large", clas: "")
    img = user.avatar.send(size).url
    image_tag(img, alt: user.name, class: "img-fluid rounded gravatar #{clas}")
  end

  def capitalize(user)
    user.name.split.each(&:capitalize!).join(" ")
  end

  def show_message(option, _user=nil)
    case option
    when :form
      base = "The form contains " + "error".pluralize(_user.errors.count)
      ["CRAP!", "#{base}<ul><li>" + _user.errors.full_messages.join("</li><li>") + "</li></ul>".html_safe]
    when :greetings
      ["Welcome #{_user.name.capitalize}", "You will enjoy our platform full of truth segrets!"]
    when :edit
      ["Nice #{_user.name.capitalize}!", "Your informations were updated!"]
    when :login_invalid
      ["CRAP!", "Invalid email/password combination"]
    when :login_required
      ["DAMN!", "You need to login to continue."]
    when :destroyed
      ["HURRA!", "I cleaned some unneeded bytes."]
    when :email
      ["ALMOST!", "Please #{_user.name.capitalize} check your email to activate your account!"]
    when :wrong_activation
      ["DAMN!!", "Invalid activation link"]
    when :non_activated
      ["ALMOST!", "Your account is not activated, please check your email for the activation link"]
    when :email_sent
      ["YEAH!", "An email was sent to your account"]
    when :email_not_found
      ["CRAP!", "Email was not found!"]
    when :success_password
      ["YUPPY!", "The password was setted correctly!"]
    when :expired
      ["F@#*!", "The link has expired"]
    when :no_posts
      ["C'MON!", "You don't have any post yet.\nPlease create one to access to that page."]
    when :no_follow
      ["ARGH!", "I know that feeling when you have that zero.."]
    when :post_saved
      ["YEAH!", "Let's take the whole world!"]
    end
  end
end
