module SessionsHelper
  # Multiple function that helps the correct flow of the session object
  # It implements basic login and logout functionalities

  def log_in(user)
    session[:user_id] = user.id
    # Updating trackables data
    user.increment_sign_in
    user.sign_in_at
    user.sign_in_ip request.remote_ip
  end

  # Remembers a user in a persistent session
  def remember(user)
    # calling the user method remember, which stores in the db the digest of the remember_token
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  def current_user?(user)
    user == current_user
  end

  def current_user
    # # The content of the session[:user_id] is assigned to a local variable called user_id
    if user_id = session[:user_id]
      # first true expression is evaluated
      # @current_user = @current_user || User.find_by(id: session[:user_id])
      # Cannot use find because it throws an error if the user is not found
      @current_user ||= User.find_by id: user_id
      # signed works as crypt and decrypt
    elsif unsigned_cookie_user_id = cookies.signed[:user_id]
      # Here it gets the user id via uncryption of the cookie
      # I need to use everywhere the find_by, coz throw exception if it does not exists!
      user = User.find_by id: unsigned_cookie_user_id
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to error_path unless current_user? @user
  end

  def correct_user?
    @user = User.find(params[:id])
    current_user? @user
  end

  # this is called from the template and checks if the user is logged, if not it checks the cookie (if existing)
  def logged_in?
    !current_user.nil?
  end

  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  def log_out
    forget current_user
    session.delete(:user_id)
    @current_user = nil
  end

  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
