module PostsHelper

  def can_delete? user
    current_user? user
  end

end
