module StaticPagesHelper
  def current_page(request, page)
    request.original_url.include? page
  end
end
