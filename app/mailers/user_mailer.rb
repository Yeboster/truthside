class UserMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation(user)
    @user = user
    options = {
      address:              ENV["MAILER_HOST"],
      port:                 ENV["MAILER_PORT"],
      user_name:            ENV["MAILER_USER"],
      password:             ENV["MAILER_PASS"],
      authentication:       :login,
      enable_starttls_auto: true
    }
    mail to: user.email, subject: "Account activation | TruthSide", delivery_method_options: options
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    options = {
      address:              ENV["MAILER_HOST"],
      port:                 ENV["MAILER_PORT"],
      user_name:            ENV["MAILER_USER"],
      password:             ENV["MAILER_PASS"],
      authentication:       :login,
      enable_starttls_auto: true
    }
    mail to: user.email, subject: "Password Reset | TruthSide", delivery_method_options: options
  end

  def notification_post(user, post)
    @user = user
    @post = post
    options = {
      address:              ENV["MAILER_HOST"],
      port:                 ENV["MAILER_PORT"],
      user_name:            ENV["MAILER_USER"],
      password:             ENV["MAILER_PASS"],
      authentication:       :login,
      enable_starttls_auto: true
    }
    mail to: user.email, subject: "Notification Post | TruthSide", delivery_method_options: options
  end
end
