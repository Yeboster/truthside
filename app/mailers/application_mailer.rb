class ApplicationMailer < ActionMailer::Base
  default from: "info@truthside.com"
  layout "mailer"

  # importing title helper
  add_template_helper(ApplicationHelper)
end
