class Question < ApplicationRecord
  belongs_to :user
  has_many :answer

  default_scope -> { order(created_at: :desc) }

  validates :content, length: {minimum: 8, maximum: 888}
end
