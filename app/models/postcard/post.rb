class Post < Postcard
  before_save :send_notification

  store :sti, accessors: :title, coder: JSON

  validates :title, presence: true, length: {minimum: 4, maximum: 88}

  private

  def send_notification
    # Send a email to all following people
    user.following.each do |u|
      u.send_notification_email(self) unless user == u
    end
  end
end
