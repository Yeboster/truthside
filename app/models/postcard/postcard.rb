class Postcard < ApplicationRecord
  belongs_to :user
  
  store :sti, accessors: %i[content picture], coder: JSON

  # stabby lambda
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader

  validates :content, presence: true, length: {minimum: 4, maximum: 1888}
  validates :user_id, presence: true
  validate  :picture_size

  private

  def picture_size
    errors[:picture] << "Should be less than 2 Mb" if picture.size > 2.megabytes
  end
end
