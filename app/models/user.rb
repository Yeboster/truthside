class User < ApplicationRecord
  has_many :posts, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :questions, dependent: :destroy
  # Aka followed
  # # This is going to return the relationship object (fw_id;fd_id)
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy
  # Aka followers
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy

  #  ! NEED TO LEARN IN DEPTH THIS CONCEPT!
  # # This is going to return the User object taken by the id of the relationship
  has_many :following, through: :active_relationships, source: :followed
  # Here the source can be omitted because Rails singularize the object to 'follower' so it is not needed
  has_many :followers, through: :passive_relationships, source: :follower
  # Cookie token
  attr_accessor :remember_token, :activation_token, :reset_token

  # Reference method
  before_create :create_activation_digest
  # Block type; with banger → self.email = self.email.downcase
  before_save :downcase_email

  # Profile Picture uploader
  mount_uploader :avatar, AvatarUploader

  validates :name, presence: true, length: {minimum: 4, maximum: 32}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum: 80},
                          format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}
  validates :password, presence: true, length: {minimum: 8, maximum: 32}, allow_nil: true

  validates :bio, presence: true, length: {minimum: 4, maximum: 140}

  validate :picture_size

  # A method that uses bcrypt gem to secure the password via a digest
  has_secure_password

  # static functions
  class << self
      # return the hash of the string
      def digest string
        # This line is an compacted if; also it is created a local var "cost" that gets the value of the "min_cost" attribute
        # After ? there is the code executed if the result is true
        # After : there is the false expression
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
      end

      # return a 22 lenth token
      def new_token
        SecureRandom.urlsafe_base64
      end
  end

  def remember
    self.remember_token = User.new_token
    update remember_digest: User.digest(remember_token)
  end

  def forget
    update remember_digest: nil
  end

  # This checks the remember digest saved on the cookie if match with the remember token
  def authenticated?(attribute, token)
    # meta programming; It can calls different methods (self.remember_digest, self.activation_digest)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def activate
    update activated: true, activated_at: Time.zone.now
  end

  def send_notification_email(post)
    UserMailer.notification_post(self, post).deliver_now
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def feed
    # Escaping the id
    Post.where("user_id = ?", id)
    # The same as 'posts'
  end

  def follow(other_user)
    # same as following.push(other_user)
    following << other_user
  end

  def unfollow(other_user)
    following.delete(other_user)
  end

  def following?(other_user)
    following.include?(other_user)
  end

  def increment_sign_in
    increment!(:sign_in_count)
  end

  def sign_in_at
    update current_sign_in_at: Time.now.utc
  end

  def sign_in_ip(ip)
    update current_sign_in_ip: ip
  end

  private

  def downcase_email
    email.downcase!
  end

  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest activation_token
  end

  def picture_size
    errors[:avatar] << "Should be less than 5 Mb." if avatar.size > 5.megabytes
  end
end