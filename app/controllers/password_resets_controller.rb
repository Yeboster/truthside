class PasswordResetsController < ApplicationController
  before_action :get_user, only: %i[edit update]
  before_action :valid_user, only: %i[edit update]
  before_action :check_expiration, only: %i[edit update]

  def new; end

  def create
    @user = User.find_by(email: params[:reset_password][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = show_message :email_sent
      redirect_to login_path
    else
      flash.now[:danger] = show_message :email_not_found
      render "new"
    end
  end

  def edit; end

  def update
    if params[:user][:password].empty?
      @user.errors.add :password, "Can't be empty"
    elsif @user.update user_params
      log_in @user
      @user.update reset_digest: nil
      flash[:success] = show_message :success_password
      redirect_to @user
    else
      render "edit"
    end
  end

  private

  def user_params
    params.require(:user).permit :password, :password_confirmation
  end

  def get_user
    @user = User.find_by email: params[:email]
  end

  def valid_user
    # it's called params[:id] because it's an auto generate resource
    redirect_to error_path unless @user && @user.activated? && @user.authenticated?(:reset, params[:id])
  end

  def check_expiration
    if @user.password_reset_expired?
      flash[:danger] = show_message :expired
      redirect_to new_password_reset_url
    end
  end
end
