class RelationshipsController < ApplicationController
  before_action :logged_in_user

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    # Adding Ajax
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    # Going to take the passive relationship of the user
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    # Adding Ajax
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
