class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @post = current_user.posts.build
      @posts = Post.paginate page: params[:page], per_page: 18
      if news = show_version(cookies)
        @news = news
      end
    end
  end

  def help; end

  def about; end

  def contact; end
end
