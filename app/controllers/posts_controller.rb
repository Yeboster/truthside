class PostsController < ApplicationController

  # Send email when the Post is created
  
  before_action :logged_in_user
  before_action :correct_user, only: [:destroy]

  # All posts of all users
  def index; end

  # Posts of a user
  def posts
    @user = User.find(params[:id])
    if @user.feed.any?
      @posts = @user.feed.paginate page: params[:page], per_page: 18
    else
      flash[:warning] = show_message :no_posts, @user
      redirect_to @user
    end
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = show_message :post_saved
    else
      flash[:danger] = show_message :form, @post
    end
    redirect_to root_url
  end

  def destroy
    # ? Why is this equal to Posts.find(params[:id]) ?
    @post.destroy
    flash[:success] = show_message :destroyed
    redirect_to request.referer || root_url
  end

  private

  def post_params
    params.require(:post).permit(%i[title content picture])
  end
    

  def correct_user
    # Used find_by because it does not throw an exception on not found post
    @post = current_user.posts.find_by(id: params[:id])
    redirect_to root_url if @post.nil?
  end
end
