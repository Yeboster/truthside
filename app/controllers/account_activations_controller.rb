class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    # There is :id because the token is in the same position as id /account_activation/:id
    # It's called id because is an automated resource in routes (only :edit)
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = show_message :greetings, user
      redirect_to user
    else
      flash[:danger] = show_message :wrong_activation, user
      redirect_to error_path
    end
  end
end
