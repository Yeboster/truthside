class UsersController < ApplicationController
  before_action :logged_in_user,  only: %i[index edit update destroy show following followers]
  before_action :correct_user,    only: %i[edit update]
  before_action :admin_user,      only: :destroy

  def index
    @users = User.where(activated: true).paginate page: params[:page], per_page: 18
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(permitted_params)
    # Updating Bio here because form is shared
    @user.bio = "I am proud to be an user of Truth Side, aren't you?"
    if @user.save
      @user.send_activation_email
      cookies.permanent[:version] = current_version
      # ! Strange behariour: when redirect the hash symbol become a hash string!
      flash[:info] = show_message(:email, @user)
      redirect_to login_path
    else
      flash.now[:danger] = show_message(:form, @user)
      render "new"
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = show_message(:destroyed)
    redirect_to users_url
  end

  def show
    @user = User.find(params[:id])
    @post = current_user.posts.build
    if news = show_version(cookies)
      @news = news
    end
    redirect_to root_url && return unless @user.activated?
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(permitted_params)
      flash[:success] = show_message(:edit, @user)
      redirect_to @user
    else
      flash.now[:danger] = show_message(:form, @user)
      render "edit"
    end
  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @all_users = @user.following
    # active_relationships is linked with Relationship class, that is why rails is searching for _relationship template
    @users = @all_users.paginate page: params[:page], per_page: 18
    if @users.any?
      render 'show_follow'
    else
      flash[:warning] = show_message(:no_follow)
      redirect_to @user
    end
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @all_users = @user.followers
    @users = @all_users.paginate page: params[:page], per_page: 18
    if @users.any?
      render 'show_follow'
    else
      flash[:warning] = show_message(:no_follow)
      redirect_to @user
    end
  end

  private

  # This method ensure the correct behaviour and minimize possible attacks, like param: admin=1
  def permitted_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :avatar, :bio)
  end

  # Before filters

  def admin_user
    redirect_to error_path unless current_user.admin?
  end
end
