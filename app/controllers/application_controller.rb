class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # This module is included here to add the helper methods to the controllers
  # otherwise it would be accessible only from the templates
  # needed for sessions login
  include SessionsHelper
  # added to shrink the code of controllers!
  include UsersHelper

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      # save the current page
      store_location
      flash[:danger] = show_message(:login_required)
      redirect_to login_url
    end
  end

  # First function ever
  def obey
    render html: "Obey to me!"
  end

  def new_user?(user)
    user.sign_in_count == 1
  end

  def current_version
    TruthSide::Application::VERSION
  end

  def new_version?(cookies)
    if cookies[:version]
      true if current_version > cookies[:version]
    else
      true
    end
  end

  def version(num)
    base = "<div class=\"text-center h4 ttf-bold mb-3\">%{ver}</div><ul class=\" list-unstyled text-left ttf-regular h5\"><li>%{body}</li></ul>"
    case num
    when v = "0.0.8"
      base.html_safe % {ver: v, body: "Optimized the internal logic and database</li><li>Improved user experience".html_safe}
    when v = "0.0.8.1"
      base.html_safe % {ver: v, body: "A notification email will be sent for new posts from the followed users!</li><li>Improved internal logic</li><li class=\"py-2\"><i>Next version will have comments under the posts!</i>".html_safe}
    end
  end

  def show_version(cookies)
    if new_user?(current_user) || new_version?(cookies)
      current_user.increment_sign_in
      cookies.permanent[:version] = current_version
      version(current_version)
    end
  end
end
