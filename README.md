# Truth Side

Welcome to the _Truth Side_!

## Basic Explanation

Well, this app was made to understand Ruby's and Rails' logic.
Initially I was following the tutorial very correctly but, after I've understood the basics, I started to do as I wanted.

I've used creativity and some stylish graphical features to make it feel like a professional site.

## Other explaination

In this project I've used sql-based databases but, in the near future, I want to understand also file-based databases, like: MongoDB.

## Compile instructions

Because pipelines are a nice thing, I've managed to write down the instructions to have them working nicely

>It compiles the assets → less work for rails on production
`rails assets:clean assets:precompile`
> If you wanna push a branch that is not master
`git push heroku yourbranch:master`
